# Mexica Libre

##Definition

Mexica Libre is an implementation, in Java, of an automatic storyteller (Mexica) based on the Engagement - Reflection creativity model.

The original software, Mexica, was build by Rafael Pérez y Pérez.
This new version, Mexica Libre, was build by Iván Guerrero Román based on the original software, but he also added new elements such as social norm analysis and diverse methodologies to evaluate the generated stories.

##Contact
For any questions, contact the author at ivangro@gmail.com